#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

from reedsolo import RSCodec
from binascii import unhexlify

alphabet="0123456789ABCDEFGHJKMOPQRSTUWXYZ"; l_alphabet=len(alphabet)
alphabet="0123456789ABCDEFGHJKMNPQRSTUWXYZ"; l_alphabet=len(alphabet)

print("Das verwendete Alphabet hat {} Zeichen.".format(l_alphabet))
Prime=1021; assert l_alphabet==32
ReverseMapping=dict()
for i in range(0,l_alphabet):
    ReverseMapping[alphabet[i]]=i

rsc=RSCodec(nsym=4)

def i2s(a : int):
    if a < l_alphabet:
        return alphabet[a]
    else:
        return i2s(a // l_alphabet) + alphabet[a % l_alphabet]

def encode(a : int):
    #print("H1",a)
    s=i2s(a)
    #print("H2",s)
    while len(s)<12:
        s="0"+s
    #print("H3",s)
    ecc  =rsc.encode(s)
    s=ecc[:-4].decode()
    ecc_s=''.join('{:02X}'.format(i) for i in ecc[-4:])

    s=s[:4]+" "+s[4:8]+" "+s[8:]+" "+ecc_s[:4]+" "+ecc_s[4:]
    return s

def s2i(s : str):
    a=0; e=0
    for i in s[::-1]:
        if not i in ReverseMapping:
            raise ValueError("invalid encoding")
        a+=ReverseMapping[i]*(l_alphabet**e)
        e+=1

    return a

# funktioniert noch nicht
def decode(s : str):
    assert len(s)>(12+4)
    s=s.replace(" ","")
    # die nächste zeile ist ein wenig ein hack
    s0=s[:-8].encode()
    s1=unhexlify(s[-8:])

    s2=rsc.decode(s0 + s1)
    a=s2i(s2)

    return a

r=1
print(encode(r))
print(encode(r+1))
print(encode(r+2))

# zufällige Zahl
r=21823156734211
print(encode(r))
print(encode(r+1))
print(encode(r+2))

#print(r, i2s(r))
#print(s2i(i2s(r)))
#print(r, encode(r))
#print(decode(encode(r)))
#
