#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

from reedsolo import RSCodec
from random import randint

def f_str(a):
    s=a[:-4].decode()
    s+=" "
    s+=''.join('{:02X}'.format(i) for i in a[-4:])
    return s

r=RSCodec(nsym=4)
n="1234567890123"

n_prime=r.encode(n)
mylen=len(n_prime)
print(f_str(n_prime))

for i in range(1,10):
    x=randint(0, mylen-1)
    y=randint(0, mylen-1)
    while x==y:
        y=randint(0, mylen-1)
    # folgend ist QnD und nur probabilistisch korrekt
    A=list(n_prime)
    A[x]=randint(0x20, 0x7e)
    A[y]=randint(0x20, 0x7e)
    n_prime_wrong=bytearray(A)

    print("Original", f_str(n_prime), "fehlkodiert", f_str(n_prime_wrong))
    o=r.decode(n_prime_wrong)
    if o:
        print("OK:", o.decode())
    else:
        print("FAIL")

