# Variante 1, IBAN ähnliche Fehlererkennung:

Für die Konstruktion des Alphabets nehme ich zunächst die Ziffern 0 bis 9,
Anschliessend nehme ich die Buchstaben (ohne Umlaute) hin zu, wobei ich nur die
Großbuchstaben verwenden. Dabei lassen ich alle Buchstaben weg, die bei
bestimmten Schriftarten sehr ähnlich aussehen und damit bei manueller Eingabe
zur Fehleingabe neigen: i,I,l,L<-\>1, O<-\>0.
Dies ist analog zur Base58-Kodierung, die bei vielen Blockchain-Systemen verwendet
wird (manuelle Eingaben von Überweisungsadressen).

Damit komme ich auf 32 Zeichen für das Alphabet. Dass diese Zahl eine 
Zweierpotenz ist, ist eher Zufall, d. h. diese Eigenschaft ist nicht
unbedingt notwendig.

alphabet="0123456789ABCDEFGHJKMNPQRSTUWXYZ"

1021 Primzahl ist die größte Primzahl die kleiner ist als 32^2 - 1

Bei einer IBAN verwendet man die Ziffern 0-9, damit ist die größte
kodierbare Zahl bei der Prüfziffer die Zahl 99. Die größte Primzahl,
die kleiner als 99 ist, ist 97. Daher verwendet man bei der IBAN
immer Modulus 97. Durch unser größeres Alphabet können wir mehr Zustände
kodieren.
Damit erwerden die Kodierungen kürzer UND wir erreichen eine bessere
Fehlererkennungsrate, da das Galois-Feld über den wir einen Morphismus
erzeugen größer ist (1021 Element vs. 97 Elemente).


    $ ./variante_1.py
    Das verwendete Alphabet hat 32 Zeichen.
    Ich kodiere 1, 2, 3
    0000 0000 01ZU
    0000 0000 02ZR
    0000 0000 03ZN
    Ich kodiere die zufällig gewählten Zahlen 21823156734211, 21823156734212, 21823156734213
    0KU4 CR2J 83AZ
    0KU4 CR2J 84AW
    0KU4 CR2J 85AS
    Kodierung und Dekodierung von 21823156734211->0KU4 CR2J 83AZ->21823156734211

# Variante 2, mit Reed-Solomon-Codes experimentieren 

Durch Verwendung der Reed-Solomon-Kodierung, so wie sie auch bei CD, DVD,
QR-Codes, DataMatrix-Codes etc. verwendet wird, kann man Fehler sowohl erkennen
und zu einem bestimmten Maße automatisch korrigieren (Forward Error Correction).
Im Beispiel werden zwei beliebige falsche Zeichen erkannt und korriert.

Der Preis ist dass die "Prüfsumme" länger sein muss als bei Variante 1 -- die nur 
Fehlererkennung und nicht automatische Korrektur leisten kann.

    $ ./variante_1.py
    Das verwendete Alphabet hat 32 Zeichen.
    0000 0000 0001 A4BA E0FF
    0000 0000 0002 B5E0 683F
    0000 0000 0003 BAD6 107F
    000K U4CR 2J83 F15D 63B7
    000K U4CR 2J84 DCDF 166A
    000K U4CR 2J85 D3E9 6E2A

