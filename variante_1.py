#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

# Für die Konstruktion des Alphabets nehme ich zunächst die Ziffern 0 bis 9,
# Anschliessend nehme ich die Buchstaben (ohne Umlaute) hinzu, wobei ich nur die
# Großbuchstaben verwenden. Dabei lassen ich alle Buchstaben weg, die bei
# bestimmten Schriftarten sehr ähnlich aussehen und damit bei manueller Eingabe
# zur Fehleingabe neigen: i,I,l,L<->1, O<->0, 
# Dies ist analog zur Base58-Kodierung, die bei vielen Blockchain-Systemen verwendet
# wird (manuelle Eingaben von Überweisungsadressen).
# 
# Damit komme ich auf 32 Zeichen für das Alphabet. Dass diese Zahl eine 
# Zweierpotenz ist, ist eher Zufall, d. h. diese Eigenschaft ist nicht
# unbedingt notwendig.

alphabet="0123456789ABCDEFGHJKMNPQRSTUWXYZ"; l_alphabet=len(alphabet)
print("Das verwendete Alphabet hat {} Zeichen.".format(l_alphabet))

# 1021 Primzahl ist die größte Primzahl die kleiner ist als 32^2 - 1
Prime=1021; assert l_alphabet==32

# Bei einer IBAN verwendet man die Ziffern 0-9, damit ist die größte
# kodierbare Zahl bei der Prüfziffer die Zahl 99. Die größte Primzahl,
# die kleiner als 99 ist, ist 97. Daher verwendet man bei der IBAN
# immer Modulus 97. Durch unser größeres Alphabet können wir mehr Zustände
# kodieren.
# Damit erwerden die Kodierungen kürzer UND wir erreichen einen bessere
# Fehlererkennungsrate, da das Galois-Feld über den wir einen Morphismus
# erzeugen größer ist (1021 Element vs. 97 Elemente).

ReverseMapping=dict()
for i in range(0,l_alphabet):
    ReverseMapping[alphabet[i]]=i

def i2s(a : int):
    if a < l_alphabet:
        return alphabet[a]
    else:
        return i2s(a // l_alphabet) + alphabet[a % l_alphabet]

def encode(a : int):
    b=a<<10; x=b%Prime
    c=b + (Prime-x+1)
    s=i2s(c)
    while len(s)<12:
        s="0"+s
    s=s[:4]+" "+s[4:8]+" "+s[8:]
    return s

def s2i(s : str):
    a=0; e=0
    for i in s[::-1]:
        if not i in ReverseMapping:
            raise ValueError("invalid encoding")
        a+=ReverseMapping[i]*(l_alphabet**e)
        e+=1

    return a

def decode(s : str):
    assert len(s)>2
    s=s.replace(" ","")
    a1=s2i(s)
    if a1%Prime!=1:
        raise ValueError("invalid encoding (checksum error)")
    return s2i(s[:-2])

print("Ich kodiere 1, 2, 3")
r=1
print(encode(r))
print(encode(r+1))
print(encode(r+2))

# zufällige Zahl
r=21823156734211
print("Ich kodiere die zufällig gewählten Zahlen {}, {}, {}".format(r, r+1, r+2))
print(encode(r))
print(encode(r+1))
print(encode(r+2))

print("Kodierung und Dekodierung von {}->{}->{}".format(r, encode(r), decode(encode(r)) ))
#print(r, i2s(r))
#print(s2i(i2s(r)))
#print(r, encode(r))
#print(decode(encode(r)))

